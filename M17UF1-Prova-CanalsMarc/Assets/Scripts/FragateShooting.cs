﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShooting : MonoBehaviour
{
    public float fireRate = 180, bulletsPerSecond;
    public bool canIShoot = true;
    public GameObject bulletPrefab;
    private GameObject bullet;
    private Transform shootpoint1, shootpoint2;
    private Rigidbody2D bulletRB;
    public float speed = 30f;
    

    // Start is called before the first frame update
    void Start()
    {
        shootpoint1 = this.gameObject.GetComponentInChildren<Transform>();
        shootpoint2 = this.gameObject.GetComponentInChildren<Transform>();
        fireRate = fireRate / 60;
        bulletsPerSecond = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();   
    }

    void Shoot()
    {
        if(bulletsPerSecond > 0)
        {
            canIShoot = false;
            bulletsPerSecond -= Time.deltaTime;
        }
        if (bulletsPerSecond <= 0)
        {
            canIShoot = true;
        }

        if (canIShoot == true)
        {
            bullet = Instantiate(bulletPrefab, shootpoint1.position + new Vector3(0, 0.5f), Quaternion.Euler(0f, 0f, 180f));
            bulletRB = bullet.GetComponent<Rigidbody2D>();
            bulletRB.AddRelativeForce(new Vector2(0, 1) * speed, ForceMode2D.Impulse);
            bullet = Instantiate(bulletPrefab, shootpoint2.position + new Vector3(0, 0.5f), Quaternion.Euler(0f, 0f, 180f));
            bulletRB = bullet.GetComponent<Rigidbody2D>();
            bulletRB.AddRelativeForce(new Vector2(0, 1) * speed, ForceMode2D.Impulse);
            bulletsPerSecond = fireRate;
        }
    }
}
