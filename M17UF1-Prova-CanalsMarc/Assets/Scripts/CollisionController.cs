﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            if (this.gameObject.CompareTag("border"))
            {
                Debug.Log("Hit border!");
                Destroy(collision.gameObject);
            }
            if (this.gameObject.CompareTag("Enemy"))
            {
                GameManager.Instance.points += 5;
                Destroy(this.gameObject);
            }
            if (this.gameObject.CompareTag("Boss"))
            {
                if (GameManager.Instance.hpBoss > 20)
                {
                    GameManager.Instance.hpBoss -= 20;
                }
                else
                {
                    GameManager.Instance.points += 20;
                    Destroy(this.gameObject);
                    GameManager.Instance.hpBoss = 100;
                }
            }
            if (collision.CompareTag("border"))
            {
                Debug.Log("Hit border!");
                if (this.gameObject.CompareTag("Enemy"))
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

       
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        if (collision.collider.CompareTag("Boss"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }

        if (collision.collider.CompareTag("border"))
        {
            Debug.Log("Hit border!");
            if (this.gameObject.CompareTag("Enemy"))
            {
                Destroy(this.gameObject);
            }
        }
    }
}
