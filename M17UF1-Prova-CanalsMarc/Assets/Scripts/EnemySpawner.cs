﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemySF, enemyFragate;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateEnemies", 0.4f, 3);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemies()
    {     
        Instantiate(enemySF, new Vector3(Random.Range(-9, 9), 12, 0), Quaternion.identity);
        Instantiate(enemyFragate, new Vector3(Random.Range(-9, 9), 12, 0), Quaternion.identity);
    }
}
