﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightController : MonoBehaviour
{
    private Transform transformSF;
    public float speed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        transformSF = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transformSF.transform.position = transformSF.transform.position + new Vector3(0, -1 * (speed * 3) * Time.deltaTime);
    }
}
