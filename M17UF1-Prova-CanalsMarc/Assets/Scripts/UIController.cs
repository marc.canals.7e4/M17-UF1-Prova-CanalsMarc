﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private Text points; 
    // Start is called before the first frame update
    void Start()
    {
        points = GameObject.Find("Points").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        points.text = "Points: "+GameManager.Instance.points;
    }
}
